var canvas = document.querySelector("#main");
var ctx = canvas.getContext("2d");

var renderCanvas = document.querySelector("#rendertarget");
var renderCtx = renderCanvas.getContext("2d");

var textImage = document.querySelector("#sprite");
var textMin = document.querySelector("#min");
var textMax = document.querySelector("#max");
var textCircles = document.querySelector("#num");

let circleimage = new Image();
let nobg = false;

function Circle() {
    this.x = 0;
    this.y = 0;
    this.endy = 0;
    this.size = 0;
};

circles = []

var xdd = ""

var cnt = 0;

function newAnimation() {
    cnt = 0;
    try {
        clearInterval(xdd)
    } catch (e) {}
    var min = Number(textMin.value);
    var max = Number(textMax.value);
    var cnum = textCircles.value;
    for (let i = 0; i<cnum; i++) {
        var c = new Circle;
        c.size = Math.random()*(max-min) + min;
        c.x = Math.random()*(72-c.size);
        c.endy = Math.random()*72+c.size*1.5;
        circles[i] = c;
    }
    circleimage.src = textImage.value;
    circleimage.onload = () => {
        render(0, ctx);
        xdd = setInterval(() => {render(0, ctx);}, 17);
    }
}

function repeatAnimation() {
    cnt = 0;
    for (let i = 0; i<circles.length; i++) {
        circles[i].y = 0;
    }
}

function render(offset, target) {
    target.clearRect(0+offset*72,0,72+offset*72,144);
    cnt += 0.01;
    if (nobg == false) {
        target.fillStyle = "black";
        target.fillRect(0,0,72,144);
    }

    for (let i = 0; i<circles.length; i++) {
        if (circles[i].y < circles[i].endy) {
            circles[i].y += (circles[i].endy - circles[i].y) / 10
        }

        let alpha = 1.5 - circles[i].y/circles[i].endy - cnt;

        target.globalAlpha = alpha < 0 ? 0 : alpha ;
        target.drawImage(circleimage, circles[i].x + 72*offset, 144-circles[i].y+circles[i].size/2, circles[i].size, circles[i].size);
        target.globalAlpha = 1;
    }
}

function exportAnimation() {
    nobg = true;
    cnt = 0;
    for (let i = 0; i<circles.length; i++) {
        circles[i].y = 0;
    }
    for (let frame = 0; frame<51; frame++) {
        render(frame, renderCtx);
    }
    try {
        saveAs(renderCanvas.toDataURL('image/png'), "anim.png");
    } catch (e) {
        alert("oopsie!!! we got a cors error because browsers have stupid \"security\" features. but its ok! right click save the canvas that appears instead~");
        renderCanvas.style.display = "block";
    }
    
    nobg = false;
}

newAnimation();

document.querySelector("#new").onclick = () => {
    newAnimation();
}

document.querySelector("#save").onclick = () => {
    exportAnimation();
}

document.querySelector("canvas").onclick = () => {
    repeatAnimation();
}